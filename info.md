[![N|Solid](http://www.tutoslive.com/img/logo_caption.png)](https://tutoslive.com)

`Attend demo class on Saturday 10th Feb 2017 at 730 PM IST. Interested candidates email to ilearn@tutoslive.com for the demo class link`
# Course Introdution
This is a full stack developer course where you will be learning HTML, CSS, JavaScript, jQuery, PHP and MySQL
### Key objectives
  - building a website with twitter bootstrap and PHP
  - implement facebook login, email notifications, message notifications and general CRUD operations.
  - learn how to design a database using MySql
  - learn git which is used everywhere for project code management
  - learn to use slack
